#include <iostream>

class A {
	int some;
public:
	void b() { std::cout << "A" << std::endl; };
};

class B : public A {
	char p;
public:
	void b() { std::cout << "B" << std::endl; };
};

int main(void)
{
	A *a1 = new A();
	std::cout << "sizeof A: (with/without virtual) " << sizeof(A) << std::endl;
	A *a2 = new B();
	B *a3 = new B();
	std::cout << "sizeof B: (with/without virtual) " << sizeof(B) << std::endl;
	// Both will use the class A's version of b(), because no
	// virtual methods are used.
	a1->b();
	a2->b();
}
