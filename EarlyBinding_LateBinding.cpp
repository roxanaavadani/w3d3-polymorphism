
#include <iostream>

int add(int firstNumber, int secondNumber)
{
  return firstNumber + secondNumber;
}

int subtract(int firstNumber, int secondNumber)
{
  return firstNumber - secondNumber;
}

int multiply(int firstNumber, int secondNumber)
{
  return firstNumber * secondNumber;
}

int main()
{
  int firstNumber;
  std::cout << "Enter a number: ";
  std::cin >> firstNumber;

  int secondNumber;
  std::cout << "Enter another number: ";
  std::cin >> secondNumber;

  int operation;
  do
  {
    std::cout << "Enter an operation (0=add, 1=subtract, 2=multiply): ";
    std::cin >> operation;
  } while (operation < 0 || operation > 2);

	int result = 0;
	switch (operation)
	{
		// call the target function directly using early binding
	case 0: result = add(firstNumber, secondNumber); break;
	case 1: result = subtract(firstNumber, secondNumber); break;
	case 2: result = multiply(firstNumber, secondNumber); break;
	}

	std::cout << "The answer is: " << result << std::endl;

	return 0;
}


//
//#include <iostream>
//
//int add(int firstNumber, int secondNumber)
//{
//  return firstNumber + secondNumber;
//}
//
//int subtract(int firstNumber, int secondNumber)
//{
//  return firstNumber - secondNumber;
//}
//
//int multiply(int firstNumber, int secondNumber)
//{
//  return firstNumber * secondNumber;
//}
//
//int main()
//{
//  int firstNumber;
//  std::cout << "Enter a number: ";
//  std::cin >> firstNumber;
//
//  int secondNumber;
//  std::cout << "Enter another number: ";
//  std::cin >> secondNumber;
//
//  int operation;
//  do
//  {
//    std::cout << "Enter an operation (0=add, 1=subtract, 2=multiply): ";
//    std::cin >> operation;
//  } while (operation < 0 || operation > 2);
//
//  // Create a function pointer named pFcn (yes, the syntax is ugly)
//  int(*pFunctionOperation)(int, int) = nullptr;
//
//  // Set pFcn to point to the function the user chose
//  switch (operation)
//  {
//  case 0: pFunctionOperation = add; break;
//  case 1: pFunctionOperation = subtract; break;
//  case 2: pFunctionOperation = multiply; break;
//  }
//
//  // Call the function that pFcn is pointing to with x and y as parameters
//  // This uses late binding
//  if (pFunctionOperation != nullptr)
//    std::cout << "The answer is: " << pFunctionOperation(firstNumber, secondNumber) << std::endl;
//  else
//    std::cout << "Te operation was not valid" << std::endl;
//
//
//  return 0;
//}

/*The compiler is unable to use early binding to resolve the function call pFcn(x, y) because it can not tell which function pFcn will be pointing to at compile time!*/