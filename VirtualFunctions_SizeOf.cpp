#include<iostream>

class A
{

};

class B
{
	int i;
};

class C
{
	void foo();
};

class D
{
	virtual void foo();
};

class E
{
	int i;
	virtual void foo();
};

class F
{
	int i;
	void foo();
};

class G
{
	void foo();
	int i;
	void foo1();
};

class H
{
	int i;
	virtual void foo();
	virtual void foo1();
};

int main()
{
	std::cout << "sizeof(class A) : " << sizeof(A) << std::endl;
	std::cout << "sizeof(class B) adding the member int i : " << sizeof(B) << std::endl;
	std::cout << "sizeof(class C) adding the member void foo() : " << sizeof(C) << std::endl;
	std::cout << "sizeof(class D) after making foo virtual : " << sizeof(D) << std::endl;
	std::cout << "sizeof(class E) after adding foo virtual , int : " << sizeof(E) << std::endl;
	std::cout << "sizeof(class F) after adding foo  , int : " << sizeof(F) << std::endl;
	std::cout << "sizeof(class G) after adding foo  , int : " << sizeof(G) << std::endl;
	G g;
	std::cout << "sizeof(class G) after adding foo  , int : " << sizeof(g) << std::endl;
	std::cout << "sizeof(class H) after adding int 2 virtual " << sizeof(H) << std::endl;
	return 0;
}
