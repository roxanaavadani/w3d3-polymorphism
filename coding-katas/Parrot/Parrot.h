#pragma once
#include <algorithm>
#include <stdexcept>

class Parrot
{
public:
    enum class Type
    {
        EUROPEAN,
        AFRICAN,
        NORWEGIAN_BLUE
    };

    Parrot(Type type, int numberOfCoconuts, double voltage, bool isInjured);

    double GetSpeed() const;
    Type GetType() const;

private:
    double GetBaseSpeed() const;
    double GetBaseSpeed(double voltage) const;
    double GetLoadFactor() const;

    Type m_type;
    int m_numberOfCoconuts;
    double m_voltage;
    bool m_isInjured;
};

Parrot* CreateParrot(Parrot::Type type, int numberOfCoconuts, double voltage, bool isInjured);
