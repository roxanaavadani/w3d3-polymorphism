#include "Parrot.h"
#include <cassert>
#include <functional>

namespace ParrotTest
{
    static void GetSpeedOfEuropeanParrot()
    {
        Parrot* parrot = CreateParrot(Parrot::Type::EUROPEAN, 0, 0, false);
        assert(12.0 == parrot->GetSpeed());
        assert(Parrot::Type::EUROPEAN == parrot->GetType());
        delete parrot;
    }

    static void GetSpeedOfAfricanParrot_With_One_Coconut()
    {
        Parrot* parrot = CreateParrot(Parrot::Type::AFRICAN, 1, 0, false);
        assert(3.0 == parrot->GetSpeed());
        assert(Parrot::Type::AFRICAN == parrot->GetType());
        delete parrot;
    }

    static void GetSpeedOfAfricanParrot_With_Two_Coconuts()
    {
        Parrot* parrot = CreateParrot(Parrot::Type::AFRICAN, 2, 0, false);
        assert(0.0 == parrot->GetSpeed());
        assert(Parrot::Type::AFRICAN == parrot->GetType());
        delete parrot;
    }

    static void GetSpeedOfAfricanParrot_With_No_Coconuts()
    {
        Parrot* parrot = CreateParrot(Parrot::Type::AFRICAN, 0, 0, false);
        assert(12.0 == parrot->GetSpeed());
        assert(Parrot::Type::AFRICAN == parrot->GetType());
        delete parrot;
    }

    static void GetSpeedNorwegianBlueParrot_injured()
    {
        Parrot* parrot = CreateParrot(Parrot::Type::NORWEGIAN_BLUE, 0, 0, true);
        assert(0.0 == parrot->GetSpeed());
        assert(Parrot::Type::NORWEGIAN_BLUE == parrot->GetType());
        delete parrot;
    }

    static void GetSpeedNorwegianBlueParrot_not_injured()
    {
        Parrot* parrot = CreateParrot(Parrot::Type::NORWEGIAN_BLUE, 0, 1.5, false);
        assert(18.0 == parrot->GetSpeed());
        assert(Parrot::Type::NORWEGIAN_BLUE == parrot->GetType());
        delete parrot;
    }

    static void GetSpeedNorwegianBlueParrot_not_injured_high_voltage()
    {
        Parrot* parrot = CreateParrot(Parrot::Type::NORWEGIAN_BLUE, 0, 4, false);
        assert(24.0 == parrot->GetSpeed());
        assert(Parrot::Type::NORWEGIAN_BLUE == parrot->GetType());
        delete parrot;
    }
}

int main()
{
    std::function<void()> allTests[] =
    {
        ParrotTest::GetSpeedOfEuropeanParrot,
        ParrotTest::GetSpeedOfAfricanParrot_With_One_Coconut,
        ParrotTest::GetSpeedOfAfricanParrot_With_Two_Coconuts,
        ParrotTest::GetSpeedOfAfricanParrot_With_No_Coconuts,
        ParrotTest::GetSpeedNorwegianBlueParrot_injured,
        ParrotTest::GetSpeedNorwegianBlueParrot_not_injured,
        ParrotTest::GetSpeedNorwegianBlueParrot_not_injured_high_voltage,
    };

    for (auto test : allTests)
    {
        test();
    }

    return 0;
}