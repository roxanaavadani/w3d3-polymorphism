#pragma once

#include <string>
#include <vector>
#include <algorithm>

class Item
{
public:
    using DayCount = int;
    using Quality = int;

    Item(const char* name, DayCount sellIn, Quality quality) :
        m_name(name), m_sellIn(sellIn), m_quality(quality) {}
    ~Item() = default;

    Item(const Item&) = default;
    Item& operator= (const Item&) = default;

    const std::string& GetName() const { return m_name; }
    DayCount GetSellIn() const { return m_sellIn; }
    Quality GetQuality() const { return m_quality; }

    void Update(DayCount sellIn, Quality quality)
    {
        m_sellIn = sellIn;
        m_quality = quality;
    }

private:
    std::string m_name;
    DayCount m_sellIn;
    Quality m_quality;
};

void UpdateItem(Item& item);

class Shop
{
public:
    Shop() : m_items() {}
    ~Shop() = default;

    Shop(const Shop&) = delete;
    Shop& operator= (const Shop&) = delete;

    void AddItem(const char* name, Item::DayCount sellIn, Item::Quality quality)
    {
        m_items.emplace_back(name, sellIn, quality);
    }

    const std::vector<Item>& Items() const { return m_items; }

    void Update()
    {
        std::for_each(m_items.begin(), m_items.end(), UpdateItem);
    }

private:
    std::vector<Item> m_items;
};

