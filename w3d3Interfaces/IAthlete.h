#pragma once
#include"IInterogate.h"
#include"IMedals.h"
#include"IParticipation.h"
class IAthlete
{
public:
	
	virtual ~IAthlete() {}
	virtual const IInterogate*  AsInterogate() const = 0;
	virtual const IParticipation* AsParticipation() const = 0;
	virtual const IMedals* AsMedals() const = 0;
};

