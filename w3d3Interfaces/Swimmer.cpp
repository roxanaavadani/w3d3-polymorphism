#include "Swimmer.h"

const IInterogate * Swimmer::AsInterogate() const
{
	return static_cast<const IInterogate*>(this);//!
}

const IParticipation * Swimmer::AsParticipation() const
{
	return static_cast<const IParticipation*>(this);//!
}

const IMedals * Swimmer::AsMedals() const
{
	return static_cast<const IMedals*>(this);//!
}

int Swimmer::NumMedals() const
{
	return numMedals;
}

int Swimmer::Participation() const
{
	return disciplines;
}

void Swimmer::Interogate() const
{
	std::cout << "My name is " << name << ", I am from " << country;
}

Swimmer::~Swimmer()
{
}
