#include "TennisPlayer.h"

const IInterogate * TennisPlayer::AsInterogate() const
{
	return static_cast<const IInterogate*>(this);//!
}

const IParticipation * TennisPlayer::AsParticipation() const
{
	return static_cast<const IParticipation*>(this);//!
}

const IMedals * TennisPlayer::AsMedals() const
{
	return static_cast<const IMedals*>(this);//!
}

int TennisPlayer::NumMedals() const
{
	return numMedals;
}

int TennisPlayer::Participation() const
{
	return disciplines;
}

void TennisPlayer::Interogate() const
{
	std::cout << "My name is " << name << ", I am from " << country;
}

TennisPlayer::~TennisPlayer()
{
}
