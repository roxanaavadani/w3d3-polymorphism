#pragma once

class IMedals
{
public:
	virtual ~IMedals() {}
	virtual int NumMedals() const = 0;
};

