#include<iostream>
#include"IInterogate.h"
#include"IParticipation.h"
#include"IMedals.h"
#include"IAthlete.h"
#include"Swimmer.h"
#include"Runner.h"
#include"TennisPlayer.h"

void main()
{
	const IAthlete** a = new const IAthlete*[3];
	a[0] = new Swimmer("S W I M M E R", "Country1", 3, 5);
	a[1] = new Runner(" R U N N E R", "Country2", 12, 6);
	a[2] = new TennisPlayer("T E N N I S", "Country3", 32, 6);

	PrintAthletes(a, 3);
}